let express = require('express');
let app = express();
const port = 3000

let routes= require('./Routes/router');
const bodyparser=require('body-parser');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.get('/', function (req, res) {
    res.send("Hello world!");
    console.log("hello world===============")
});
//app.use(express);
app.use('/api',routes);
//app.use(express.json)
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})