const express = require('express');
const crudController = require('../Conroller/CRUD');

const router = express.Router();
router.route('/create').post(crudController.Create);
router.route('/read').get(crudController.Read);
router.route('/update').post(crudController.Update);
router.route('/delete').post(crudController.Delete);


module.exports=router;
